from flask import Flask # imports the flask module to use in this app
from flask import render_template
import os, sys

dir = os.path.split(os.path.abspath(os.path.realpath(sys.argv[0])))[0]

def logPostRequest(ip, form):
    up = ('%s Username:%s, Password:%s\n' % (ip, form['log'], form['pwd'],))
    with open(dir + '/modules/wp_admin_handler/args.dat', 'a') as f:
        f.write(up)
def wpAdmin():
    return render_template('wp-admin.php')

def wpLogin():
    return render_template('wp-login.php')
def wordPressAdminHandler(request):
    if bool(request.form):
        logPostRequest(request.remote_addr, request.form)
    if request.path == '/wp-login.php':
        return wpLogin()
    elif request.path == '/wp-admin':
        return wpAdmin()
