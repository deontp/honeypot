from flask import Flask # imports the flask module to use in this app
from flask import render_template
import os, sys

dir = os.path.split(os.path.abspath(os.path.realpath(sys.argv[0])))[0]

def logPostRequest(args):
    with open(dir + '/modules/sqli_handler/args.dat', 'a') as f:
        f.write(args.get('id')+'\n')

def processArgs(args):
    logPostRequest(args)
    if "'" in args.get('id'):
        return 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near  \'\'\'\'\' at line 1'
    else:
        return render_template('index.html')

def sqliHandler(request):
    return processArgs(request.args)
