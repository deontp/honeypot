import logging.config # to use a config file for logging
import logging # to use the standard pyton logger
import datetime
import pytz # this is used to enable us to get the timezone information when outputting the date to access logs
from pytz import timezone # so that we can change the time zone of pytz
import os, sys

STATS = {}
REQUEST = None
ALOG = None
RLOG = None
dir = os.path.split(os.path.abspath(os.path.realpath(sys.argv[0])))[0]
LOCAL_TIME = timezone('Africa/Johannesburg') # see for info on what this does http://pytz.sourceforge.net/
DATE_TIME = None

def loadStats():
    "retrievs stats from a file name stats.dat"
    if os.path.isfile(dir + '/data/stats.dat'):
        for i in open(dir + '/data/stats.dat', 'r').read().split('\n'):
            if i != '':
                temp = i.split('=')
                STATS[temp[0]] = temp[1]

def saveStats():
    "saves the stats from dictionary to stats.dat"
    if not os.path.exists(dir + '/data'):
        os.makedirs(dir + '/data')
    with open(dir + '/data/stats.dat', 'w') as f:
        for l in STATS:
            f.write('%s=%d\n' % (l,STATS[l]))

def getLogger(loggerName):
    "retunrs a log file to be used"
    if not os.path.exists(dir + '/logs'):
        os.makedirs(dir + '/logs')
    logging.config.fileConfig(dir + '/modules/default_logger/logging.conf') # uses an external logging file to get configuration information
    return logging.getLogger(loggerName) # sets log to be the request logger from the logging.conf file

def logPostRequest():
    filename = 'req' + str(STATS['REQUESTS']) + '.post'
    fname = DATE_TIME.strftime('%d%m%Y%H%M%S') + 'r' + str(STATS['REQUESTS']) + '.post'
    # filename = date_time_requestID.post
    data = REQUEST.get_data()
    if not os.path.exists(dir + '/logs/post'):
        os.makedirs(dir + '/logs/post')
    with  open(dir + '/logs/post/' + fname, 'w') as f:
        f.write('%s\nArgs:\n%s\nForm:\n%sData:%s' % (REQUEST.headers, REQUEST.args, REQUEST.form, data,) )

def logRequest(): # keep track of the requests that come in and the number of requests that have been recieved
    "logs the requests to the webpage as well as the request counter"
    global RLOG
    if STATS == {}:
        loadStats()
    if 'REQUESTS' in STATS.keys():
        STATS['REQUESTS'] = int(STATS['REQUESTS']) + 1
    else:
        STATS['REQUESTS'] = 1
    args = {'src':REQUEST.remote_addr, 'pid':STATS['REQUESTS'], 'headers':REQUEST.headers}
    if RLOG == None:
        RLOG = getLogger('request_logs')
    RLOG.info('[%(pid)s] %(src)s \n%(headers)s'  % args)
    saveStats()

def logCLF ():#(logFile, request): # log files in common log format as per appache logs
    " logs a request in the appache common log format"
    global ALOG
    res = ''.join(REQUEST.path)
    if bool(REQUEST.args):
        res += '?'
        for key in REQUEST.args:
            if not res.endswith('?'):
                res += '&'
            res += key + '=' + REQUEST.args[key]
    format = '%d/%b/%Y:%H:%M:%S %z' # see http://strftime.org/ for date time placeholder

    args = {'src': REQUEST.remote_addr,
    'method': REQUEST.method,
    'resource': res,
    'http': REQUEST.environ['SERVER_PROTOCOL'],
    'code': '-', 'rfc':'-',
    'userid':'-',
    'size':'-',
    'date': DATE_TIME.strftime(format)} # logs every request that comes into the server
    if ALOG == None:
        ALOG = getLogger('access_logs')
    ALOG.info('%(src)s %(rfc)s %(userid)s [%(date)s] \"%(method)s %(resource)s %(http)s\" %(code)s %(size)s'  % args)

def loggingHandler(request):
    global REQUEST, ALOG, RLOG, DATE_TIME # must use global when assiging a value to a global variable
    DATE_TIME = datetime.datetime.today().replace(tzinfo=LOCAL_TIME)
    REQUEST = request
    ALOG = getLogger('access_logs')
    RLOG = getLogger('request_logs')
    logCLF()
    logRequest()
    if REQUEST.method == 'POST':
        logPostRequest() # log the post request information to see what POST's are coming in.
