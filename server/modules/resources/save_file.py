from werkzeug import secure_filename # to handle file uploads
import os, sys, hashlib

STORED_FILES = {}
dir = os.path.split(os.path.abspath(os.path.realpath(sys.argv[0])))[0]

def getStoredFiles():
    "Reads the exisiting stored filenames and hashes from a file"
    if os.path.isfile(dir + '/modules/file_upload_handler/filenames.dat'):
        with open(dir + '/modules/file_upload_handler/filenames.dat', 'r') as f:
            for line in f:
                temp = line.replace('\n', '').split(':')
                if not len(temp) == 1: # accounts for an empty file
                    STORED_FILES[temp[0]] = temp[1].split(',')

def saveStoredFiles():
    "saves the filenames from dictionary to stats.info"
    tempString = ""
    with open(dir + '/modules/file_upload_handler/filenames.dat', 'w+') as f:
        for l in STORED_FILES:
            tempString += l + ":" # adds the hash value to the string
            for item in STORED_FILES[l]:
                tempString += item + ',' # adds the list item to the string
            tempString = tempString[:-1] # removes the last , from the string
            tempString += "\n" # puts each string on a new line
        f.write(tempString)

def saveFile(file):
    "This function processes the POST request and saves the uploaded file to the server"
    if STORED_FILES == {}:
        getStoredFiles() # gets all the files stored on the server
    if file:
        filename = secure_filename(file.filename) # Make the filename safe, remove unsupported chars
        fileContents = file.read()
        hashValue = hashlib.sha1(fileContents).hexdigest() # hash the file
        #hashValue = hashString(fileContents)
        file.close()
        if hashValue in STORED_FILES:
            if filename not in STORED_FILES[hashValue]: # checks if the filename is already in the dictionary with the same hash if it is not it adds it
                STORED_FILES[hashValue].append(filename)
        else:
            if not os.path.exists(dir + '/uploads'):
                os.makedirs(dir + '/uploads')
            STORED_FILES[hashValue] = [filename] # save the filename to STORED_FILES
            open(os.path.join(dir + '/uploads/', hashValue), "w").write(fileContents) # save the file to disk
    saveStoredFiles()
