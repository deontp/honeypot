from flask import Flask # imports the flask module to use in this app
from flask import render_template
import os, sys

FILES = {'stats':'/data/stats', 'access_logs' : '/logs/access.log', 'error_logs' : '/logs/error.log', 'request_logs' : '/logs/requests.log', 'filenames': '/modules/file_upload_handler/filenames.dat'}

dir = os.path.split(os.path.abspath(os.path.realpath(sys.argv[0])))[0]
def reportingHandler(request):
    reportNumber = request.args.get('report')
    result = 'No reports to show'
    if reportNumber :
        return returnFile(FILES.keys()[int(reportNumber)])
    reports = ''
    for item in FILES.keys():
        reports += '%d: %s<br>' % (FILES.keys().index(item), item,)
    return render_template('reports.html', reports=reports)

def returnFile(file): # called using md5 has of acceslogs
    "retrievs request from access logs"
    result = ''
    tempList = open(dir + FILES[file], 'r').read().split('\n')
    for i in tempList:
        if i != '':
            result += '%s<br>' % i
    return result
