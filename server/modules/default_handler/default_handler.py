from flask import Flask # imports the flask module to use in this app
from flask import render_template
from modules.resources import save_file


def noHandler(request):
    if request.method == 'POST' and bool(request.files):
        save_file.saveFile(request.files['file'])
    return render_template('default.html', path=request.path)
