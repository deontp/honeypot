#------------------------------------------------------------------------------------#
#-------------------------------REQUIRED MODULES-------------------------------------#
#------------------------------------------------------------------------------------#
# pip install jinja2 > for templating
#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#

from flask import Flask # imports the flask module to use in this app
from flask import request # allows us to see the request headers
import os # for os task such as getting the working directory
import logging # to turn off logging to console

# Custom modules for my honeypot
from modules.default_logger import default_logger
from modules.default_handler import default_handler
from modules.root_handler import root_handler
from modules.file_upload_handler import file_upload_handler
from modules.wp_admin_handler import wp_admin_handler
from modules.stats_handler import stats_handler
from modules.reporting_handler import reporting_handler


SPECIAL_PAGES = ['stats', 'logs/access']

app = Flask(__name__) # creates an instance of the Flask class and calls it app

# With these routes setup we are catching *any* possible URL/request.
# The root URL/domain for the app (/) is catched by the first route
# While the second route using the path placeholder will catch any
# route like /login, /logout, or even /foo/bar.
@app.route('/', defaults={'path':'/'}) # defines what must be done when a request comes in for "/"
@app.route('/<path:path>')
def getRequests(path):
    getPaths = {'/' : root_handler.rootHandler,'file.php' : file_upload_handler.fileUploadHandler,'wp-admin' : wp_admin_handler.wordPressAdminHandler,' wp-login.php' : wp_admin_handler.wordPressAdminHandler,'stats' : stats_handler.displayStats,'logs/access' : reporting_handler.displayAccessLogs,}

    if path not in SPECIAL_PAGES:
        default_logger.logCLF(clog, request)
       	default_logger.logRequest(rlog, request)

    if path in SPECIAL_PAGES and not request.args.get('mode') == 'report':
        return default_handler.noHandler(path), 203

    if path in getPaths: # handle any implemented modules
        return getPaths[path](), 200
    else: # handle any paths not implemented
        return default_handler.noHandler(path), 404

@app.route('/<path:path>', methods=['POST'])
def postRequest(path):
    postPaths = {}
    default_logger.logCLF(clog, request)
    default_logger.logRequest(rlog, request)
    #To access form data (data transmitted in a POST or PUT request) you can use the form attribute
    return file_upload_handler.uploadFile(request), 200

if __name__ == "__main__": # __name__ is the name attirbute of the objects
    clog = default_logger.getLogger('access_logs') # creates a new log object to write logs to in the common log format style
    rlog = default_logger.getLogger('request_logs') # log file for requests
    elog = default_logger.getLogger('error_logs') # log errors that occur
    logging.getLogger('werkzeug').setLevel(logging.INFO) # log only errors, closest I can get to turning it off
    app.logger.addHandler(elog)
    app.run(host="146.231.123.9", port=80,debug=False)
