import os
import sys
# create a .py file adding the modules and paths from the config file to the template
configFile = []
dir = os.path.split(os.path.abspath(os.path.realpath(sys.argv[0])))[0]
template = open(dir + '/data/template.py', 'r').read()
output= open(dir + '/server.py', "w")

# reads in the config file with modules to use
with open(dir + '/data/config','r') as f:
    for line in f:
        if not line[0] == '#':
            configFile.append(line.replace('\r\n', ''))

importString = ""
getPathString = ""
postPathString = ""

for item in configFile:
    tmp = item.split(" ") # split into sections
    tmp[1] = tmp[1].replace("/",".")
    importString += "from " + tmp[1] + " import " + tmp[2] + "\n"
    if tmp[0] == 'GET':
        getPathString += "'" + tmp[3] + "' : " + tmp[2] + "." + tmp[4].replace('\n','') + ','
    elif tmp[0] == 'POST':
        postPathString += "'" + tmp[3] + "' : " + tmp[2] + "." + tmp[4].replace('\n','') + ','

template = template.replace("<IMPORTS>", importString)
template = template.replace("<GET-PATHS>", getPathString)
template = template.replace("<POST-PATHS>", postPathString)
output.write(template)

output.flush()
output.close()
