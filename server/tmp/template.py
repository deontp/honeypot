#-------------------------------REQUIRED MODULES-------------------------------------#
#------------------------------------------------------------------------------------#
# pip install jinja2 > for templating
# pip install flask > for flask
#------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------#

from flask import Flask # imports the flask module to use in this app
from flask import request # allows us to see the request headers
import os # for os task such as getting the working directory
import logging # to turn off logging to console
from modules.default_logger import default_logger
from modules.default_handler import default_handler
<IMPORTS>

#Globals------------------------------------------
HOST = '146.231.123.9'
PORT = 8080
DEBUG = True
ADMIN_IP = '146.231.123.9'
SPECIAL_PAGES = ['honeypot/reports']
#-------------------------------------------------

app = Flask(__name__) # creates an instance of the Flask class and calls it app

# With these routes setup we are catching *any* possible URL/request.
# The root URL/domain for the app (/) is caught by the first route
# While the second route using the path placeholder will catch any
# route like /login, /logout, or even /foo/bar.
# Can handle GET and POST requests
@app.route('/', defaults={'path':'/'}, methods=['GET', 'POST']) # defines what must be done when a request comes in for "/"
@app.route('/<path:path>', methods=['GET', 'POST']) # catch all paths
def getRequests(path):
    paths = {<PATHS>}
    if not request.remote_addr == ADMIN_IP: # log all requests that are not made from ADMIN_IP
        default_logger.loggingHandler(request)
    if path in SPECIAL_PAGES and not request.remote_addr == ADMIN_IP: # if a request comes for a special page but not from ADMIN_IP call noHandler
        return default_handler.noHandler(request), 404
    if path in paths: # and request.method == 'GET': # handle any implemented modules
        return paths[path](request), 200
    else: # handle any paths not implemented
        return default_handler.noHandler(request), 404

if __name__ == "__main__": # __name__ is the name attirbute of the objects
#    clog = default_logger.getLogger('access_logs') # creates a new log object to write logs to in the common log format style
#    rlog = default_logger.getLogger('request_logs') # log file for requests
    elog = default_logger.getLogger('error_logs') # log errors that occur
    logging.getLogger('werkzeug').setLevel(logging.INFO) # log only errors, closest I can get to turning it off
    app.logger.addHandler(elog)
    app.run(host=HOST, port=PORT,debug=DEBUG)
