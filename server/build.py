# create a .py file adding the modules and paths from the config file to the template
import os
import sys
import ConfigParser

# Globals
configFile = []
importString = ""
pathString = ""

dir = os.path.split(os.path.abspath(os.path.realpath(sys.argv[0])))[0]
template = open(dir + '/tmp/template.py', 'r').read()
Config = ConfigParser.ConfigParser()
Config.read(dir + '/modules/modules.config')

def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

for item in ConfigSectionMap('modules')['keys'].split(','):
    tmp = ConfigSectionMap(item)
    importString += "from " + tmp['path'].replace("/",".") + " import " + item + "\n"

modules = Config.sections()
modules.remove('modules')

for m in modules:
    tmp = ConfigSectionMap(m)
    if tmp['method'] == 'GET':
        for a in tmp['resource'].split(','):
            pathString += "'" + a.strip() + "' : " + m + "." + tmp['handler'] + ','
    elif tmp['method'] == 'POST':
        postPathString += "'" + tmp['resource'] + "' : " + m + "." + tmp['handler']+ ','
#
template = template.replace("<IMPORTS>", importString)
template = template.replace("<PATHS>", pathString)


# write out the .py file with the implemented modules
output= open(dir + '/server.py', "w+")
output.write(template)
output.flush()
output.close()
